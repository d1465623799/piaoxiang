<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/book.css" />

    <title></title>
</head>
<body>

<div class="navbar navbar-default">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <ul class="nav navbar-nav dh">
                    <li><a href="#">文学类</a></li>
                    <li><a href="#">历史类</a></li>
                    <li><a href="#">艺术类</a></li>
                    <li><a href="#">法律类</a></li>
                    <li><a href="#">军事类</a></li>
                    <li><a href="#">童书类</a></li>
                    <li><a href="#">体育类</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--
    轮播图
-->
<div id="div1">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="4000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="../img/011.jpg" >

            </div>
            <div class="item">
                <img src="../img/012.jpg" >

            </div>
            <div class="item">
                <img src="../img/013.jpg" >

            </div>
            <div class="item">
                <img src="../img/014.jpg" >

            </div>
            <div class="item">
                <img src="../img/015.jpg" >

            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" id="aaron1"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" id="aaron2"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--
        作者：offline
        时间：2019-10-16
        描述：图书展示部分
    -->
    <div class="container">
        <div class="row">
            <div class="col-md-9 books">
                <p class="tj">好书推荐/<a href="#" class="gd">更多</a>

                </p>
                <div style="height:2px;width:870px;border-top:1px solid #ccc;float:left;margin-top:15px;"></div>
                <div class="col-md-3  book_lb">
                    <ul>
                        <li><a href="#"><img src="img/books/0101.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥12.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0102.jpg" /></a></li>
                        <li  class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0103.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0104.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                    </ul>
                </div>
                <div class="col-md-3 book_lb">
                    <ul>
                        <li><a href="#"><img src="img/books/0301.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0410.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲asadasdasdasdasdasd</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0107.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0501.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                    </ul>
                </div>
                <div class="col-md-3 book_lb">
                    <ul>
                        <li><a href="#"><img src="img/books/0109.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0506.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0509.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0610.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                    </ul>
                </div>
                <div class="col-md-3 book_lb">
                    <ul>
                        <li><a href="#"><img src="img/books/0204.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0708.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0710.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                        <li><a href="#"><img src="img/books/0805.jpg" /></a></li>
                        <li class="jj"><a href="#">三体：地球往事三部曲</a></li>
                        <span>刘慈欣 著</span>
                        <p>￥30.00&nbsp;<span>起</span></p>
                    </ul>
                </div>
            </div>




            <script type="text/javascript" src="../js/jquery-1.12.4.js" ></script>
            <script type="text/javascript" src="../js/bootstrap.js" ></script>

            <script>
                $('li.dropdown').mouseover(function() {
                    $(this).addClass('open');
                })
                    .mouseout(function() {
                        $(this).removeClass('open');
                    });


            </script>

</body>
</html>
