<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>搜索结果页</title>
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/booklist.css" />
    <link rel="stylesheet" href="../css/searchPage.css"/>
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/searchPage.js"></script>
    <link rel="icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
</head>
<body>
<!--导航条-->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <input type="hidden" id="p_userid" value="${user.id}">
    <div class="container">
        <div class="navbar-header">
            <a href="../index/main.jsp" class="navbar-brand glyphicon glyphicon-home">首页</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav  navbar-right">
                <li>
                    <c:if test="${user != null }">
                        <a href="../pc/pc_top.jsp"> Hello：${user.name}</a>
                    </c:if>
                    <c:if test="${user == null }">
                        <a href="../login_register/login_register.jsp">
                            登录/注册
                        </a>
                    </c:if>

                </li>
                <li>
                    <a id="shoppingcar">购物车</a>
                </li>
                <li>
                    <a href="#">我的订单</a>
                </li>
                <li>
                    <a href="../pc/pc_top.jsp">个人中心
                    </a>

                </li>
                <li>
                    <a href="../index/booklist.jsp">图书详情</a>
                </li>
                <li>
                    <a href="#">服务</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--搜索框-->
<div class="container div_body">
    <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="input-group">
                <input type="text" id="searchBook" placeholder="请输入关键词..." class="form-control input-lg">
                <span id="search"  class="input-group-addon btn btn-danger">搜索</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="box" id="box">
            <ul >

            </ul>
        </div>
    </div>
</div>
<div class="container div_jieGuo">
    <div class="row">
        <div class="col-md-2" >
            共<span class="total"></span>条搜索结果
        </div>
        <div class="col-md-4 col-md-offset-1">
            没有想要的结果？试试&nbsp;<a href="../index/booklist.jsp">分类查看</a>
        </div>
    </div>
</div>
<div class="container div_neiRong">
    <div class="body">
        <div class="book-all ">

        </div>
    </div>
    <!-- 分页 -->
    <div class="row text-center">
        <nav aria-label="Page navigation">
        </nav>
    </div>
</div>

</body>
</html>
