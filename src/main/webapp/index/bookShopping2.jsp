<%--
  Created by IntelliJ IDEA.
  User: xulei
  Date: 2019/11/11
  Time: 9:43 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录后空购物车</title>
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <style type="text/css">
        * {
            margin: 0px;
            padding: 0px;
            font-size: 14px;
            color: #666666;
        }

        .shoppingCar {
            margin: 200px auto;
            width: 500px;
            height: 80px;
            text-align: center;
        }
        .shoppingCar img{
            height: 100px;
            width: 100px;
        }
    </style>
</head>
<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<div class="shoppingCar"><img src="../img/shoppingCar.png">您的购物车是空的，您可以
    <a href="../index/main.jsp">点击这里</a>去挑选图书
</div>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>
</body>
</html>
