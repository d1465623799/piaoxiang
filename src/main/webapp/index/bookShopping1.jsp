
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/public_top.css" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <title>未登录账号时购物车</title>
    <style type="text/css">
        * {
            margin: 0px;
            padding: 0px;
            font-size: 14px;
            color: #666666;
        }

        .shoppingCar {
            margin: 200px auto;
            width: 500px;
            height: 80px;
            text-align: center;
        }
        .shoppingCar img{
            height: 100px;
            width: 100px;
        }
    </style>

</head>

<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<div class="shoppingCar"><img src="../img/shoppingCar.png">您的购物车是空的，您可以
    <a href="../login_register/login_register.jsp">立即登录</a>
</div>
<jsp:include page="../public/public_bottom.jsp"></jsp:include>
</body>

</html>