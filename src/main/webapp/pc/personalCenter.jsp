<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" href="../css/public_bottom.css" />
    <style>
        *{
            margin: 0 auto;

            font-size: 12px;
        }
        body{
            background-color: #F5F5F4;
        }
        .box{
            width: 100%;
            height: 36px;
            background-color: white;
        }
        .header{
            width: 1000px;
            height: 36px;
            text-align: center;
        }
        .search{
            width: 1000px;
            height: 100px;
            text-align: center;
            background-color: white;
            margin-top: 20px;
        }
        .link-box{
            height: 36px;
            float: left;
        }
        .info-box{
            height: 36px;
            float: right;
        }
        .item-info{
            float: left;
            position: relative;
            top: 10px;
            margin-left: 20px;
        }
        .center-logo{
            font-weight: 200;
            height: 62px;
        }
        .center-logo-box{
            float: left;
            position: relative;
            top: 20px;
            left: 30px;

        }
        .input-group{
            width: 400px;
            height: 40px;
            position: relative;
            left: 170px;

        }
        .form-control{
            width: 400px;
            height: 40px;
        }
        .btn{
            width: 98px;
            height: 40px;
            font-size: 20px;
            background-color: #8C222C;
        }
        .form-control,.btn{
            position: relative;
            top: 30px;
        }
        .content{
            width: 1000px;
            height: 300px;
            text-align: center;
            margin-top: 20px;
            /*border: 1px solid red;*/
        }
        .left-bar{
            float: left;
            background-color: white;
        }
        .right-bar{
            width: 810px;
            float: right;
            padding: 40px;
            background-color: white;
        }
        .head-picture{
            width: 110px;
            height: 110px;
            border-radius: 55px;
            margin: 10px 30px;
        }
        .menu{
            list-style-type: none;
            position: relative;
            right: 20px;
            margin:15px;
        }
        .menu-text{
            text-align: center;
            font-size: 15px;
            color: #88888C;
        }
        .left-line-margin{
            border: 1px solid #8A202A;
        }
        .lead-text{
            width: 170px;
            margin-bottom: 10px;
        }
        .user-name-text{
            position: relative;
            right: 20px;
        }
        .sign-out{
            position: relative;
            left: 20px;
        }
        .title-line-margin{
            border: 1px solid #dbdbdb;
            margin: 20px 0px;
        }
        .personal-title{
            position: relative;
            right: 300px;
        }
        .lead-picture{
            width: 170px;
        }
        .lead-picture{
            float: left;
        }
        .info-list{
            width: 560px;
            float: right;
            /*border: 1px solid red;*/
        }
        .basic-title{
            float: left;
        }
        .personal-update{
            float: right;
            position: relative;
            top: 10px;
        }
        .basic-info{
            list-style-type: none;
            margin: 10px;
        }
        .text-line-margin{
            width: 560px;
            height: 1px;
            background: #dbdbdb;
            transform:scaleY(0.5);
            float: left;
        }
        .basic-info1,.basic-info2,.basic-info3{
            list-style-type: none;
        }
        .basic-info-text{
            float: left;
            margin-top: 10px;
        }
    </style>
    <title></title>
</head>
<body>

<div class="box">

    <div class="header">
        <div class="link-box">
            <div class="item-info">
                <a href="#" class="info-link">
                    <span class="info-text">首页</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#" class="info-link">
                    <span class="info-text">书店</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#" class="info-link">
                    <span class="info-text">书摊</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#" class="info-link">
                    <span class="info-text">新书</span>
                </a>
            </div>
        </div>

        <div class="info-box">
            <div class="item-info">
                <a href="#">
                    <span class="info-text">无耻下流</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#">
                    <span class="info-text">消息</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#">
                    <span class="info-text">购物车</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#">
                    <span class="info-text">我的订单</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#">
                    <span class="info-text">个人中心</span>
                </a>
            </div>
            <div class="item-info">
                <a href="#">
                    <span class="info-text">联系客服</span>
                </a>
            </div>
        </div>
    </div>

    <div class="search">

        <div class="center-logo-box">
            <a href="#">
                <img class="center-logo" src="img/personalCenterlogo.jpg" alt="中国领先书籍交易平台" />
            </a>
        </div>

        <div class="input-group">
            <input type="text" class="form-control" placeholder="商品名称、作者、出版社" />
            <span class="input-group-btn">
						<a href="#" class="btn btn-primary">搜索</a>
					</span>
        </div>

    </div>

    <div class="content">

        <div class="left-bar">
            <div class="lead-tit">
                <a href="#" ><img class="head-picture" src="img/H-picture.jpg" /></a>
            </div>
            <div class="lead-text" >
                <a href="#" style="color: #333333;" class="user-name-text">[list]</a>
                <a href="#" style="color: #333333;" class="sign-out"><span>[退出登录]</span> </a>
            </div>
            <div class="left-line-margin"></div>
            <div class="menu-box">
                <ul class="uer-list-menu">
                    <li class="menu">
                        <a class="menu-text" href="#">我的订单</a>
                    </li>
                    <li class="menu">
                        <a class="menu-text" href="#">评价晒单</a>
                    </li>
                    <li class="menu">
                        <a class="menu-text" href="#">地址管理</a>
                    </li>
                    <li class="menu">
                        <a class="menu-text" href="#">个人信息</a>
                    </li>
                    <li class="menu">
                        <a class="menu-text" href="#">修改密码</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="right-bar">
            <div class="personal-title">
                <h3>个人信息</h3>
            </div>
            <div class="title-line-margin"></div>
            <div class="lead-picture">
                <a href="#" ><img class="head-picture" src="img/H-picture.jpg" /></a>
                <br />
                <a href="#" class="lead-text" style="color: #333333;" >修改头像</a>
            </div>
            <div class="info-list">
                <div class="info-title">
                    <h5 class="basic-title">基础资料</h5>
                    <a href="#" class="personal-update" >编辑</a>
                </div>
                <div class="text-line-margin"></div>
                <p class="basic-info-text">姓名：lisi</p>
                <div class="text-line-margin"></div>
                <p class="basic-info-text">生日：1995-01-01</p>
                <div class="text-line-margin"></div>
                <p class="basic-info-text">性别：男</p>
                <div class="text-line-margin"></div>
            </div>

        </div>

    </div>

</div>

<script type="text/javascript" src="js/jquery-1.12.4.js" ></script>
<script type="text/javascript" src="js/bootstrap.js" ></script>
</body>
</html>
