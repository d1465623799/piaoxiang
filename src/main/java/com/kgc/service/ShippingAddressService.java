package com.kgc.service;

import com.kgc.entity.ShippingAddress;

import java.util.List;

public interface ShippingAddressService {
    //查询用户下用没有收货地址
    int queryCountByUserId (Integer userid);
    //查询用户下的所有收货地址
    List<ShippingAddress> queryAddressById(Integer userid);
    //给用户增加一个收货地址
    int addAddress(ShippingAddress address);
    //通过addressid查询该地址
    ShippingAddress queryAddressByAddressId(Integer id);
    // 根据id删除地址
    int delAddressById(int id);
    // 修改地址
    int updateAddressById(ShippingAddress address);
}
