package com.kgc.service.impl;

import com.kgc.entity.Book;
import com.kgc.entity.BookType;
import com.kgc.entity.ShoppingCar;
import com.kgc.mapper.BookMapper;
        import com.kgc.service.BookService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;


    /**
     * 查询所有图书
     */
    @Override
    public List<Book> queryBook() {
        return bookMapper.queryBook();
    }

    /**
     * 增加图书
     */
    @Override
    public int addBook(Book book) {
        return bookMapper.addBook(book);
    }

    /**
     * 查询所有图书分类
     */
    @Override
    public List<BookType> queryBookType() {
        return bookMapper.queryBookType();
    }

    /**
     * 通过id删除该图书
     */
    @Override
    public int delBookById(int id) {
        return bookMapper.delBookById(id);
    }

    /**
     * 通过id查询该图书
     */
    @Override
    public Book queryBookById(int id) {
        return bookMapper.queryBookById(id);
    }


    /**
     * 通过id更新该图书
     */
    @Override
    public int updateBookById(Book book) {
        return bookMapper.updateBookById(book);
    }

    /**
     * 分类获取书
     * @param bookType
     * @return
     */
    @Override
    public List<Book> queryBookByType(int bookType) {
        return bookMapper.queryBookByType(bookType);
    }

    @Override
    public List<Book> queryBookBySearch(String searchBook) {
        return bookMapper.queryBookBySearch(searchBook);
    }

    @Override
    public int updataBookNumber(Integer bookid, Integer bookNumber) {
        return bookMapper.updataBookNumber(bookid,bookNumber);
    }



}
