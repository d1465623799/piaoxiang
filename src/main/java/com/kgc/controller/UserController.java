package com.kgc.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.*;
import com.kgc.service.UserService;
import com.kgc.utils.CookieUtil;
import com.kgc.utils.Random4;
import com.kgc.utils.SendSms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
    String code;

    @Autowired
    private UserService userService;

    /**
     * 根据用户名和密码查询用户
     * @return
     */
    @RequestMapping("login")
    public  int queryByNameAndPassword(String tel, String password, HttpServletRequest request){
        User user = userService.queryByNameAndPassword(tel, password);
        if (user != null) {
            request.getSession().setAttribute("user",user);
            if (user.getRoleid()==1){
                return 2;
            }
            return 1;

        }

        return 3;
    }

    /**
     * 验证码登录
     */
    @RequestMapping("clogin")
    public boolean clogin(String tel,HttpServletRequest request){
            User user=userService.queryByTel(tel);
            if (user!=null) {
                request.getSession().setAttribute("user", user);
                code=null;

                return true;

            }else {
                return false;
            }
    }


    /**
     * 验证手机号是否已注册
     * @param tel
     * @return
     */
    @RequestMapping("isexisttel")
    public boolean isexisttel(String tel){
        if(userService.queryTel().contains(tel)){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 短信验证码
     * @param phoneNo
     * @return
     */
    @RequestMapping("sendSms")
    public boolean sendSms(String phoneNo){
        code = Random4.getRandom();
        while (code.length()<4){
            code = Random4.getRandom();
        }
        /*System.out.println(code);*/
        return SendSms.sendSms(phoneNo,code);
        /*return true;*/
    }

    /**
     * 验证码校验
     * @param usercode
     * @return
     */
    @RequestMapping("confirmup")
    public boolean confirmup(String usercode){
        return code.equals(usercode);
    }

    /**
     * 注册
     * @param user
     * @param usercode
     * @return
     */
    @RequestMapping("register")
    public int register(User user,String usercode,HttpServletRequest request){
        user.setRoleid(2);
        user.setHead("../img/head/pc_book.jpg");
        user.setSex("*");
        user.setAge(0);
        if(code.equals(usercode)){
            if(userService.register(user)>=1){
                code=null;
                userService.queryIdByTel(user.getTel());
                request.getSession().setAttribute("user",user);
                return 1;
            }else{
                return 2;
            }
        }else{
            return 3;
        }

    }

    /**
     * 查询所有用户并分页
     * @param pn
     * @param ps
     * @return
     */
    @RequestMapping("selectAll")
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps){
        PageHelper.startPage(pn,ps);
        List<User> users = userService.queryUser();
        PageInfo info = new PageInfo(users,5);
        return info;
    }

    /**
     * 查询所有用户分类
     * @return
     */
    @RequestMapping("queryuserRole")
    public List<UserRole> queryUserRole(){
        return userService.queryUserRole();
    }

    /**
     * 控制台增加用户
     * @param user
     * @return
     */
    @RequestMapping("adduser")
    public boolean adduser(User user){
        if (userService.adduser(user)>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 通过id删除该用户
     * @param id
     * @return
     */
    @RequestMapping("delUserById")
    public boolean deluser(int id){
        if (userService.deluser(id)>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 通过id查找该用户信息
     * @param id
     * @return
     */
    @RequestMapping("queryUserById")
    public User queryUserById(int id){
        return userService.queryUserById(id);
    }

    /**
     * 通过id更新该用户
     * @param user
     * @return
     */
    @RequestMapping("updateuser")
    public  boolean updateUserById(User user) {
        if (userService.updateUser(user) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 查询收货地址
     * @param id
     * @return
     */
    @RequestMapping("queryAddress")
    public List<ShippingAddress> queryAddress(int id) {
        return userService.queryAddress(id);
    }

    /**
     * 完善或修改信息
     * @param user
     * @return
     */
    @RequestMapping("completeInfo")
    public  boolean completeInfo(User user, HttpServletRequest request){
        boolean flag = userService.completeInfo(user)>=1?true:false;
        if(flag){
           User newuser= queryUserById(user.getId());
           request.getSession().setAttribute("user",newuser);
        }
        return flag;
    }

    /**
     * 添加cookie
     * @param key
     * @param val
     * @param response
     */
    @RequestMapping("addCookie")
    public void addCookie(String key,String val,HttpServletResponse response){
        CookieUtil.setCookie(key,val,30,response);
    }

    /**
     * 清除session并跳转
     */
    @RequestMapping("clear")
    public void  clear(HttpServletRequest request, HttpServletResponse response){
        request.getSession().invalidate();
        CookieUtil.setCookie("auto","",0,response);
        try {
            response.sendRedirect(request.getContextPath()+"/index/main.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 清除session并跳转login
     */
    @RequestMapping("clearlogin")
    public void  clearlogin(HttpServletRequest request, HttpServletResponse response){
        request.getSession().invalidate();
        CookieUtil.setCookie("auto","",0,response);
        try {
            response.sendRedirect("/piaoxiang/login_register/login_register.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 修改密码
     * @param id
     * @param password
     * @return
     */
    @RequestMapping("updatePassword")
    public boolean updatePassword(int id,String password){
        code=null;
        return userService.updatePassword(id,password)>=1?true:false;
    }


    /**
     * 查询所有用户并分页
     * @param pn
     * @param ps
     * @return
     */
    @RequestMapping("adminsearchUser")
    public PageInfo admin_searchUser(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps, String searchUser){
        PageHelper.startPage(pn,ps);
        List<User> users = userService.admin_searchUser(searchUser);
        PageInfo info = new PageInfo(users,5);
        return info;
    }

    @RequestMapping("queryidbytel")
    public int queryIdByTel(String tel){
        User user = userService.queryIdByTel(tel);
        return  user.getId();

    }


    /**
     * 通过用户id获取购物车订单图书详情
     * @param userId
     * @return
     */
    @RequestMapping("queryShopByUserId")
    public List<ShoppingCar> queryShopByUserId(int userId){
        List<ShoppingCar> shoppingCars = userService.queryShopByUserId(userId);
        return shoppingCars;
    }

    /**
     * 添加收藏
     * @param userId
     * @param bookId
     * @return
     */
    @RequestMapping("addFavorite")
    public boolean addFavorite(int userId, int bookId) {
        return userService.addFavorite(userId,bookId)>=1?true:false;
    }


    /**
     * 确认收藏
     * @param userId
     * @param bookId
     * @return
     */
    @RequestMapping("confirmFavorite")
    public boolean confirmFavorite(int userId, int bookId){
        return userService.confirmFavorite(userId,bookId);

    }

    /**
     * 删除收藏
     * @param userId
     * @param bookId
     * @return
     */
    @RequestMapping("delFavorite")
    public boolean delFavorite(int userId, int bookId) {
        return userService.delFavorite(userId,bookId)>=1?true:false;
    }

    /**
     * 查询收藏
     * @param userId
     * @return
     */
    @RequestMapping("queryFavorite")
    public List<Integer> queryFavorite(int userId) {
        return userService.queryFavorite(userId);
    }

    /**
     * 删除购物车图书
     * @param userId
     * @param id
     * @return
     */
    @RequestMapping("delShopCarBook")
    public boolean delShopCarBook(int userId, int id){
        return userService.delShopCarBook(userId,id)>=1?true:false;
    }


    /**
     * 增加购物车
     * @param shoppingCar
     * @return
     */
    @RequestMapping("addShoppingCar")
    public boolean addShopppingCar(ShoppingCar shoppingCar){
        if (queryShopCarByUseridAndBookid(shoppingCar.getUser_id(), shoppingCar.getBook_id())) {
            return false;
        }else {
            return userService.addShoppingCar(shoppingCar) == 1 ? true : false;
        }
    }

    /**
     *通过用户id和图书id判断图书是否重复
     * @param userId
     * @param bookId
     * @return
     */
    @RequestMapping("queryShopCarByUseridAndBookid")
    public boolean queryShopCarByUseridAndBookid(int userId, int bookId){
        return userService.queryShopCarByUseridAndBookid(userId,bookId) >=1?true:false;
    }
}
